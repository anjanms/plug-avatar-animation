module.exports = function(grunt) {

    grunt.task.registerTask('update:manifest', 'Get latest manifest from plug and update avatars-manifest.css', function() {
        var done = this.async(),
            manifestURL = grunt.option('manifest'),
            manifestName = manifestURL.split('/').pop(),
            request = require('request');

        if (manifestURL === undefined) grunt.fail.warn('Manifest file not specified');

        grunt.log.subhead('Downloading manifest');

        grunt.log.writeln('Connecting to plug.dj');

        request.get(manifestURL, function(err, res, body) {
            if (err) return grunt.fail.warn(err.stack);

            var baseURL = body.match(/var base_url\s?=\s?(?:'|")(.+)(?:'|");/i)[1],
                manifest = JSON.parse(body.match(/var manifest\s?=\s?({.+});/i)[1]),
                manifestSize = Object.keys(manifest).length,
                output = '/*! THIS FILE WAS GENERATED ON ' + new Date().toUTCString() + ' USING ' + manifestName + ' WITH ' + manifestSize + ' AVATARS */\n',
                cssClass;

            grunt.log.writeln('Got ' + manifestName + ' with ' + manifestSize + ' avatars');

            grunt.log.subhead('Processing manifest');

            for (var id in manifest) {
                var cssClass = isNaN(id.charAt(0)) ? id : '\\3' + id.charAt(0) + ' ' + id.substring(1);
                output += '\n';
                output += '.' + cssClass + '.small {background-image: url(' + baseURL + '/' + id + '.' + manifest[id][''] + '.png);}\n';
                output += '.' + cssClass + '.big {background-image: url(' + baseURL + '/' + id + 'b.' + manifest[id]['b'] + '.png);}\n';
                output += '.' + cssClass + '.dj {background-image: url(' + baseURL + '/' + id + 'dj.' + manifest[id]['dj'] + '.png);}\n';
            }

            grunt.log.writeln('Writing file');

            grunt.file.write('./src/avatar-manifest.css', output);

            grunt.log.writeln('Done!');

            done();
        });

    });

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: {
            bin: ['./bin/']
        },
        cssmin: {
            options: {
                processImport: false
            },
            animation: {
                options: {
                    keepSpecialComments: 1
                },
                src: './src/avatar-animation.css',
                dest: './bin/avatar-animation.min.css'
            },
            manifest: {
                src: './src/avatar-manifest.css',
                dest: './bin/avatar-manifest.min.css'
            }
        },
        replace: {
            manifestImport: {
                src: ['./bin/avatar-animation.min.css'],
                overwrite: true,
                replacements: [{from: 'avatar-manifest.css', to: 'avatar-manifest.min.css'}]
            }
        }
    });

    grunt.loadNpmTasks('grunt-text-replace');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('build', ['clean:bin', 'cssmin:animation', 'cssmin:manifest', 'replace:manifestImport']);

    grunt.registerTask('update', ['update:manifest', 'build']);

    grunt.registerTask('default', 'build');
};