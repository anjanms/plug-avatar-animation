# Plug Avatar Animation
### Animates plug.dj avatars outside plug.dj

Usage:
```
<div class="plug-avatar small blink ba01"></div>
<div class="plug-avatar big dance ba02"></div>
<div class="plug-avatar dj dance dragon-e01"></div>
```

** Avatar Types: **

`small`
`big`
`dj`

** Avatar Animations: **

`blink`
`dance`

_Only `dance` is supported on dj avatars_

** Avatar ID **

These are available in the css file

_Classes starting with numbers are escaped, so `.\32 014hw01` becomes `2014hw01`._